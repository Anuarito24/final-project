package com.epam.tcfp.ave.controller;

import com.epam.tcfp.ave.service.Service;
import com.epam.tcfp.ave.service.factory.ServiceFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

public class AveController extends HttpServlet {
	private final ServiceFactory serviceFactory = ServiceFactory.getInstance();
	static Logger logger = LogManager.getLogger();
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String requestUri = request.getRequestURI().toLowerCase();
		Service currentService = serviceFactory.getService(requestUri);
		try {
			currentService.execute(request, response);
		} catch (ParseException | SQLException e) {
			logger.log(Level.ERROR, "Exception in Controller");
			logger.info(e.getClass().getName());
			logger.info(e.getMessage());
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
