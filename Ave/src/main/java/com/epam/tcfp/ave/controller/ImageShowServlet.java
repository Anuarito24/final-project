package com.epam.tcfp.ave.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class ImageShowServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OutputStream os = response.getOutputStream();
        String pictureName = request.getParameter("work_pictureName");
        String path = "D:\\Ave\\uploads\\";
        if (!pictureName.equals("")) {
            path += pictureName;
        }
        else {
            path += "notExist.png";
        }
        File file = new File(path);
        FileInputStream fips = new FileInputStream(file);
        byte[] btImg = readStream(fips);
        os.write(btImg);
        os.flush();
    }

    /**
     * Reading Stream Data in Pipeline
     */
    public byte[] readStream(InputStream inStream) {
        ByteArrayOutputStream bops = new ByteArrayOutputStream();
        int data = -1;
        try {
            while((data = inStream.read()) != -1){
                bops.write(data);
            }
            return bops.toByteArray();
        }catch(Exception e){
            return null;
        }
    }
}
