package com.epam.tcfp.ave.database.dao;

import com.epam.tcfp.ave.database.connection.ConnectionPool;
import com.epam.tcfp.ave.database.interfaces.LanguageDao;
import com.epam.tcfp.ave.entity.Language;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LanguageDaoImpl implements LanguageDao {
    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public Integer getLanguageId(String language) throws SQLException {
        int id = 0;
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("select * from language where language=?")) {
            ps.setString(1, language);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return id;
    }

    @Override
    public List<Language> getAll() throws SQLException {
        List<Language> languages = new ArrayList<>();
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("select * from language")) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Language language = new Language();
                language.setId(rs.getInt("id"));
                language.setLanguage(rs.getString("language"));
                languages.add(language);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return languages;
    }

}
