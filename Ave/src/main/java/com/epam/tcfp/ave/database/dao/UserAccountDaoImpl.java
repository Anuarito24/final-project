package com.epam.tcfp.ave.database.dao;

import com.epam.tcfp.ave.database.connection.ConnectionPool;
import com.epam.tcfp.ave.database.interfaces.UserAccountDao;
import com.epam.tcfp.ave.entity.UserAccount;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserAccountDaoImpl implements UserAccountDao {
	private ConnectionPool connectionPool;
	private Connection connection;

	@Override
	public boolean addUserAccount(UserAccount userAccount) throws SQLException {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("insert into userAccount(login, email, password, " +
					"create_date, role, banned) values (?, ?, ?, ?, ?, ?)")) {
			ps.setString(1, userAccount.getLogin());
			ps.setString(2, userAccount.getEmail());
			ps.setString(3, userAccount.getPassword());
			ps.setDate(4, new java.sql.Date(userAccount.getCreateDate().getTime()));
			ps.setString(5, userAccount.getRole());
			ps.setBoolean(6, userAccount.getBanned());
			ps.executeUpdate();
		} finally {
			connectionPool.returnConnection(connection);
		}
		return true;
	}

	@Override
	public void delete(Long id) throws SQLException {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("delete from userAccount where id=?")){
			UserDetailsDaoImpl userDetailsDao = new UserDetailsDaoImpl();
			ps.setLong(1, id);
			userDetailsDao.delete(id);
			ps.executeUpdate();
		} finally {
			connectionPool.returnConnection(connection);
		}
	}

	@Override
	public void update(UserAccount userAccount) throws SQLException {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("update userAccount set role=?, banned=? where id=?")){
			ps.setString(1, userAccount.getRole());
			ps.setBoolean(2, userAccount.getBanned());
			ps.setLong(3,userAccount.getId());
			ps.executeUpdate();
		} finally {
			connectionPool.returnConnection(connection);
		}
	}

	@Override
	public List<UserAccount> getAll() throws SQLException {
		List<UserAccount> employees = new ArrayList<>();
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from userAccount")){
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserAccount userAccount = new UserAccount();
				userAccount.setId(rs.getLong("id"));
				userAccount.setLogin(rs.getString("login"));
				userAccount.setEmail(rs.getString("email"));
				userAccount.setCreateDate(rs.getDate("create_date"));
				userAccount.setRole(rs.getString("role"));
				userAccount.setBanned(rs.getBoolean("banned"));
				employees.add(userAccount);
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return employees;
	}

	@Override
	public UserAccount getUserAccountByLoginPassword(String login, String password) throws SQLException {
		UserAccount userAccount = new UserAccount();
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from userAccount where login=?")){
			ps.setString(1, login);
			ResultSet rs = ps.executeQuery();
			if (rs.next() && rs.getString("password").equals(password)) {
				userAccount.setId(rs.getLong("id"));
				userAccount.setLogin(rs.getString("login"));
				userAccount.setEmail(rs.getString("email"));
				userAccount.setPassword(rs.getString("password"));
				userAccount.setCreateDate(rs.getDate("create_date"));
				userAccount.setRole(rs.getString("role"));
				userAccount.setBanned(rs.getBoolean("banned"));
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return userAccount;
	}

	@Override
	public UserAccount getUserAccountById(long id) throws SQLException {
		UserAccount userAccount = new UserAccount();
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from userAccount where id=?")){
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				userAccount.setId(rs.getLong("id"));
				userAccount.setLogin(rs.getString("login"));
				userAccount.setEmail(rs.getString("email"));
				userAccount.setPassword(rs.getString("password"));
				userAccount.setCreateDate(rs.getDate("create_date"));
				userAccount.setRole(rs.getString("role"));
				userAccount.setBanned(rs.getBoolean("banned"));
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return userAccount;
	}

	@Override
	public boolean isLoginExist(String login) throws SQLException {
		boolean isExist = false;
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement preparedStatement = connection.prepareStatement("select * from userAccount where login = ?")) {
			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				isExist = true;
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return isExist;
	}

	@Override
	public boolean isEmailExist(String email) throws SQLException {
		boolean isExist = false;
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement preparedStatement = connection.prepareStatement("select * from userAccount where email = ?")) {
			preparedStatement.setString(1, email);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				isExist = true;
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return isExist;
	}
}

