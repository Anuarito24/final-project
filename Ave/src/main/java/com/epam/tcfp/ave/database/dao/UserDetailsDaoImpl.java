package com.epam.tcfp.ave.database.dao;

import com.epam.tcfp.ave.database.connection.ConnectionPool;
import com.epam.tcfp.ave.database.interfaces.UserDetailsDao;
import com.epam.tcfp.ave.entity.UserDetails;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDetailsDaoImpl implements UserDetailsDao {
    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public void addUserDetails(UserDetails userDetails) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("insert into userDetails(firstName, lastName, " +
                "gender, phone, occupation, work_experience, user_id) values (?, ?, ?, ?, ?, ?, ?)")){
            ps.setString(1, userDetails.getFirstname());
            ps.setString(2, userDetails.getLastname());
            ps.setString(3, userDetails.getGender());
            ps.setString(4, userDetails.getPhone());
            ps.setString(5, userDetails.getOccupation());
            ps.setString(6, userDetails.getWorkExperience());
            ps.setLong(7, userDetails.getUserId());
            ps.executeUpdate();
        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public UserDetails getUserDetailsByUserId(long user_id) throws SQLException {
        UserDetails userDetails = new UserDetails();
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("select * from userDetails where user_id=?")){
            ps.setLong(1, user_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                userDetails.setId(rs.getLong("id"));
                userDetails.setFirstname(rs.getString("firstName"));
                userDetails.setLastname(rs.getString("lastName"));
                userDetails.setGender(rs.getString("gender"));
                userDetails.setPhone(rs.getString("phone"));
                userDetails.setOccupation(rs.getString("occupation"));
                userDetails.setWorkExperience(rs.getString("work_experience"));
                userDetails.setUserId(rs.getLong("user_id"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return userDetails;
    }

    @Override
    public void update(UserDetails userDetails) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("update userDetails set firstName=?, lastName=?, " +
                "gender=?, phone=?, occupation=?, work_experience=? where id=?")){
            ps.setString(1, userDetails.getFirstname());
            ps.setString(2, userDetails.getLastname());
            ps.setString(3, userDetails.getGender());
            ps.setString(4, userDetails.getPhone());
            ps.setString(5, userDetails.getOccupation());
            ps.setString(6, userDetails.getWorkExperience());
            ps.executeUpdate();
        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public List<UserDetails> getAll() throws SQLException {
        List<UserDetails> employees = new ArrayList<>();
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("select * from userDetails")) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserDetails userDetails = new UserDetails();
                userDetails.setId(rs.getLong("id"));
                userDetails.setFirstname(rs.getString("firstName"));
                userDetails.setLastname(rs.getString("lastName"));
                userDetails.setGender(rs.getString("gender"));
                userDetails.setPhone(rs.getString("phone"));
                userDetails.setOccupation(rs.getString("occupation"));
                userDetails.setWorkExperience(rs.getString("work_experience"));
                userDetails.setUserId(rs.getLong("user_id"));
                employees.add(userDetails);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return employees;
    }

    @Override
    public void delete(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("delete from userDetails where user_id=?")){
            ps.setLong(1, id);
            ps.executeUpdate();
        } finally {
            connectionPool.returnConnection(connection);
        }
    }
}
