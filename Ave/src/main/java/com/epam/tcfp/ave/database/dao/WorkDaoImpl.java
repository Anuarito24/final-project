package com.epam.tcfp.ave.database.dao;

import com.epam.tcfp.ave.database.connection.ConnectionPool;
import com.epam.tcfp.ave.database.interfaces.WorkDao;
import com.epam.tcfp.ave.entity.Work;
import com.epam.tcfp.ave.entity.WorkHistory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkDaoImpl implements WorkDao {
	private ConnectionPool connectionPool;
	private Connection connection;

	@Override
	public boolean addWork(Work work) throws SQLException {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("insert into work_employee(company_name, " +
				"phone_number, city, street_address, home_number, apartment_number, description, payment, user_id)" +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?)")){
			ps.setString(1, work.getCompanyName());
			ps.setString(2, work.getPhoneNumber());
			ps.setString(3, work.getCity());
			ps.setString(4, work.getStreetAddress());
			ps.setString(5, work.getHomeNumber());
			ps.setString(6, work.getApartmentNumber());
			ps.setString(7, work.getDescription());
			ps.setString(8, work.getPayment());
			ps.setLong(9, work.getUserId());
			ps.executeUpdate();
		} finally {
			connectionPool.returnConnection(connection);
		}
		return true;
	}

	@Override
	public List<Work> getAll() throws SQLException {
		List<Work> works = new ArrayList<>();
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from work_employee")){
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Work work = new Work();
				work.setId(rs.getLong("id"));
				work.setCompanyName(rs.getString("company_name"));
				work.setPhoneNumber(rs.getString("phone_number"));
				work.setCity(rs.getString("city"));
				work.setStreetAddress(rs.getString("street_address"));
				work.setHomeNumber(rs.getString("home_number"));
				work.setApartmentNumber(rs.getString("apartment_number"));
				work.setDescription(rs.getString("description"));
				work.setPayment(rs.getString("payment"));
				work.setPictureName(rs.getString("picture_name"));
				works.add(work);
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return works;
	}

	@Override
	public List<Work> getAllAvailableWork(List<WorkHistory> workHistoryList) throws SQLException {
		List<Work> works = new ArrayList<>();
		boolean result;
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from work_employee")){
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = false;
				Work work = new Work();
				work.setId(rs.getLong("id"));
				work.setCompanyName(rs.getString("company_name"));
				work.setPhoneNumber(rs.getString("phone_number"));
				work.setCity(rs.getString("city"));
				work.setStreetAddress(rs.getString("street_address"));
				work.setHomeNumber(rs.getString("home_number"));
				work.setApartmentNumber(rs.getString("apartment_number"));
				work.setDescription(rs.getString("description"));
				work.setPayment(rs.getString("payment"));
				work.setPictureName(rs.getString("picture_name"));
				for (WorkHistory workHistory : workHistoryList) {
					if (workHistory.getWorkId() == work.getId()) {
						result = true;
						break;
					}
				}
				if (!result) {
					works.add(work);
				}
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return works;
	}

	@Override
	public Work getWorkById(long id) throws SQLException {
		Work work = new Work();
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from work_employee where id=?")) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				work.setId(rs.getLong("id"));
				work.setCompanyName(rs.getString("company_name"));
				work.setPhoneNumber(rs.getString("phone_number"));
				work.setCity(rs.getString("city"));
				work.setStreetAddress(rs.getString("street_address"));
				work.setHomeNumber(rs.getString("home_number"));
				work.setApartmentNumber(rs.getString("apartment_number"));
				work.setDescription(rs.getString("description"));
				work.setPayment(rs.getString("payment"));
				work.setPictureName(rs.getString("picture_name"));
				work.setUserId(rs.getLong("user_id"));
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return work;
	}

	@Override
	public Work getLastWork() throws SQLException {
		Work work = new Work();
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("select * from work_employee order by id desc LIMIT 1")) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				work.setId(rs.getLong("id"));
				work.setCompanyName(rs.getString("company_name"));
				work.setPhoneNumber(rs.getString("phone_number"));
				work.setCity(rs.getString("city"));
				work.setStreetAddress(rs.getString("street_address"));
				work.setHomeNumber(rs.getString("home_number"));
				work.setApartmentNumber(rs.getString("apartment_number"));
				work.setDescription(rs.getString("description"));
				work.setPayment(rs.getString("payment"));
				work.setPictureName(rs.getString("picture_name"));
				work.setUserId(rs.getLong("user_id"));
			}
		} finally {
			connectionPool.returnConnection(connection);
		}
		return work;
	}
	@Override
	public void update(Work work) throws SQLException {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("update work_employee set company_name=?, " +
				"phone_number=?, city=?, street_address=?, home_number=?, apartment_number=?, description=?," +
				"payment=?, picture_name=?, user_id=? where id=?")) {
			ps.setString(1, work.getCompanyName());
			ps.setString(2, work.getPhoneNumber());
			ps.setString(3, work.getCity());
			ps.setString(4, work.getStreetAddress());
			ps.setString(5, work.getHomeNumber());
			ps.setString(6, work.getApartmentNumber());
			ps.setString(7, work.getDescription());
			ps.setString(8, work.getPayment());
			ps.setString(9, work.getPictureName());
			ps.setLong(10, work.getUserId());
			ps.setLong(11,work.getId());
			ps.executeUpdate();
		} finally {
			connectionPool.returnConnection(connection);
		}
	}

	@Override
	public void delete(Long id) throws SQLException {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.takeConnection();
		try (PreparedStatement ps = connection.prepareStatement("delete from work_employee where id=?")){
			ps.setLong(1, id);
			ps.executeUpdate();
		} finally {
			connectionPool.returnConnection(connection);
		}
	}
}
