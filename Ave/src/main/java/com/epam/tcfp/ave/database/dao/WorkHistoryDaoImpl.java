package com.epam.tcfp.ave.database.dao;

import com.epam.tcfp.ave.database.connection.ConnectionPool;
import com.epam.tcfp.ave.database.interfaces.WorkHistoryDao;
import com.epam.tcfp.ave.entity.WorkHistory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkHistoryDaoImpl implements WorkHistoryDao {
    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public boolean addWorkHistory(WorkHistory workHistory) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("insert into work_history(user_id, " +
                "work_id, login, full_name, company_name, description, status_work) values (?, ?, ?, ?, ?, ?, ?)")){
            ps.setLong(1, workHistory.getUserId());
            ps.setLong(2, workHistory.getWorkId());
            ps.setString(3, workHistory.getLogin());
            ps.setString(4, workHistory.getFullName());
            ps.setString(5, workHistory.getCompanyName());
            ps.setString(6, workHistory.getDescription());
            ps.setString(7, workHistory.getStatusWork());
            ps.executeUpdate();
        } finally {
            connectionPool.returnConnection(connection);
        }
        return true;
    }

    @Override
    public List<WorkHistory> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<WorkHistory> workHistoryList = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("select * from work_history")){
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                WorkHistory workHistory = new WorkHistory();
                workHistory.setId(rs.getLong("id"));
                workHistory.setUserId(rs.getLong("user_id"));
                workHistory.setWorkId(rs.getLong("work_id"));
                workHistory.setLogin(rs.getString("login"));
                workHistory.setFullName(rs.getString("full_name"));
                workHistory.setCompanyName(rs.getString("company_name"));
                workHistory.setDescription(rs.getString("description"));
                workHistory.setStatusWork(rs.getString("status_work"));
                workHistoryList.add(workHistory);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return workHistoryList;
    }

    @Override
    public List<WorkHistory> getAllWorkHistoryByUserId(long userId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<WorkHistory> workHistoryList = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("select * from work_history where user_id=?")){
            ps.setLong(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                WorkHistory workHistory = new WorkHistory();
                workHistory.setId(rs.getLong("id"));
                workHistory.setUserId(rs.getLong("user_id"));
                workHistory.setWorkId(rs.getLong("work_id"));
                workHistory.setLogin(rs.getString("login"));
                workHistory.setFullName(rs.getString("full_name"));
                workHistory.setCompanyName(rs.getString("company_name"));
                workHistory.setDescription(rs.getString("description"));
                workHistory.setStatusWork(rs.getString("status_work"));
                workHistoryList.add(workHistory);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return workHistoryList;
    }

    @Override
    public WorkHistory getWorkHistoryById(long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        WorkHistory workHistory = new WorkHistory();
        try (PreparedStatement ps = connection.prepareStatement("select * from work_history where id=?")){
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                workHistory.setId(rs.getLong("id"));
                workHistory.setUserId(rs.getLong("user_id"));
                workHistory.setWorkId(rs.getLong("work_id"));
                workHistory.setLogin(rs.getString("login"));
                workHistory.setFullName(rs.getString("full_name"));
                workHistory.setCompanyName(rs.getString("company_name"));
                workHistory.setDescription(rs.getString("description"));
                workHistory.setStatusWork(rs.getString("status_work"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return workHistory;
    }

    @Override
    public void update(WorkHistory workHistory) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("update work_history set status_work=? where id=?")){
            ps.setString(1, workHistory.getStatusWork());
            ps.setLong(2,workHistory.getId());
            ps.executeUpdate();
        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void delete(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        try (PreparedStatement ps = connection.prepareStatement("delete from work_history where id=?")){
            ps.setLong(1, id);
            ps.executeUpdate();
        } finally {
            connectionPool.returnConnection(connection);
        }
    }
}
