package com.epam.tcfp.ave.database.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface BaseDao<T> {
    List<T> getAll() throws SQLException;

    void delete(Long id) throws SQLException;

    void update(T object) throws SQLException;
}
