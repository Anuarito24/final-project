package com.epam.tcfp.ave.database.interfaces;

import com.epam.tcfp.ave.entity.Language;

import java.sql.SQLException;
import java.util.List;

public interface LanguageDao {

    Integer getLanguageId(String language) throws SQLException;

    List<Language> getAll() throws SQLException;


}
