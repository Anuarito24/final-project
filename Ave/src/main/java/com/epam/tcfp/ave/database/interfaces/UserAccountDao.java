package com.epam.tcfp.ave.database.interfaces;

import com.epam.tcfp.ave.entity.UserAccount;

import java.sql.SQLException;
import java.util.List;

public interface UserAccountDao extends BaseDao<UserAccount>{

    boolean addUserAccount(UserAccount userAccount) throws SQLException;

    void delete(Long id) throws SQLException;

    void update(UserAccount userAccount) throws SQLException;

    UserAccount getUserAccountByLoginPassword(String login, String password) throws SQLException;

    UserAccount getUserAccountById(long id) throws SQLException;

    boolean isLoginExist(String login) throws SQLException;

    boolean isEmailExist(String login) throws SQLException;
}
