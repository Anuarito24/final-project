package com.epam.tcfp.ave.database.interfaces;

import com.epam.tcfp.ave.entity.UserDetails;

import java.sql.SQLException;
import java.util.List;

public interface UserDetailsDao extends BaseDao<UserDetails>{

    void addUserDetails(UserDetails userDetails) throws SQLException;

    UserDetails getUserDetailsByUserId(long user_id) throws SQLException;
}
