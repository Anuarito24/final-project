package com.epam.tcfp.ave.database.interfaces;

import com.epam.tcfp.ave.entity.Work;
import com.epam.tcfp.ave.entity.WorkHistory;

import java.sql.SQLException;
import java.util.List;

public interface WorkDao extends BaseDao<Work>{

    boolean addWork(Work work) throws SQLException;

    List<Work> getAllAvailableWork(List<WorkHistory> workHistoryList) throws SQLException;

    Work getWorkById(long id) throws SQLException;

    Work getLastWork() throws SQLException;
}
