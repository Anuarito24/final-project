package com.epam.tcfp.ave.database.interfaces;

import com.epam.tcfp.ave.entity.WorkHistory;

import java.sql.SQLException;
import java.util.List;

public interface WorkHistoryDao extends BaseDao<WorkHistory>{

    boolean addWorkHistory(WorkHistory workHistory) throws SQLException;

    List<WorkHistory> getAllWorkHistoryByUserId(long userId) throws SQLException;

    WorkHistory getWorkHistoryById(long id) throws SQLException;
}
