package com.epam.tcfp.ave.entity;

import java.util.Date;
import java.util.Objects;

public class UserAccount extends AbstractEntity {
	private String login;
	private String email;
	private String password;
	private Date createDate;
	private String role;
	private Boolean banned;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateDate() {
		this.createDate = new Date();
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Boolean getBanned() {
		return banned;
	}

	public void setBanned(Boolean banned) {
		this.banned = banned;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		UserAccount that = (UserAccount) o;
		return Objects.equals(login, that.login) &&
				Objects.equals(email, that.email) &&
				Objects.equals(password, that.password) &&
				Objects.equals(createDate, that.createDate) &&
				Objects.equals(role, that.role) &&
				Objects.equals(banned, that.banned);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), login, email, password, createDate, role, banned);
	}

	@Override
	public String toString() {
		return "UserAccount{" +
				"login='" + login + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", createDate=" + createDate +
				", role='" + role + '\'' +
				", banned=" + banned +
				'}';
	}
}
