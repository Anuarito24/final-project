package com.epam.tcfp.ave.entity;

import java.util.Objects;

public class Work extends AbstractEntity {
	private String companyName;
	private String phoneNumber;
	private String city;
	private String streetAddress;
	private String homeNumber;
	private String apartmentNumber;
	private String description;
	private	String payment;
	private String pictureName;
	private Long userId;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getHomeNumber() {
		return homeNumber;
	}

	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}

	public String getApartmentNumber() {
		return apartmentNumber;
	}

	public void setApartmentNumber(String apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getPictureName() {
		return pictureName;
	}

	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Work work = (Work) o;
		return Objects.equals(companyName, work.companyName) &&
				Objects.equals(phoneNumber, work.phoneNumber) &&
				Objects.equals(city, work.city) &&
				Objects.equals(streetAddress, work.streetAddress) &&
				Objects.equals(homeNumber, work.homeNumber) &&
				Objects.equals(apartmentNumber, work.apartmentNumber) &&
				Objects.equals(description, work.description) &&
				Objects.equals(payment, work.payment) &&
				Objects.equals(pictureName, work.pictureName) &&
				Objects.equals(userId, work.userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), companyName, phoneNumber, city, streetAddress, homeNumber, apartmentNumber, description, payment, pictureName, userId);
	}

	@Override
	public String toString() {
		return "Work{" +
				"companyName='" + companyName + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				", city='" + city + '\'' +
				", streetAddress='" + streetAddress + '\'' +
				", homeNumber='" + homeNumber + '\'' +
				", apartmentNumber='" + apartmentNumber + '\'' +
				", description='" + description + '\'' +
				", payment='" + payment + '\'' +
				", pictureName='" + pictureName + '\'' +
				", userId=" + userId +
				'}';
	}
}
