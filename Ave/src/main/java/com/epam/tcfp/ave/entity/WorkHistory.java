package com.epam.tcfp.ave.entity;

import java.util.Objects;

public class WorkHistory extends AbstractEntity {
    long userId;
    long workId;
    String login;
    String fullName;
    String companyName;
    String description;
    String statusWork;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getWorkId() {
        return workId;
    }

    public void setWorkId(long workId) {
        this.workId = workId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatusWork() {
        return statusWork;
    }

    public void setStatusWork(String statusWork) {
        this.statusWork = statusWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkHistory that = (WorkHistory) o;
        return userId == that.userId &&
                workId == that.workId &&
                Objects.equals(login, that.login) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(companyName, that.companyName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(statusWork, that.statusWork);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, workId, login, fullName, companyName, description, statusWork);
    }

    @Override
    public String toString() {
        return "WorkHistory{" +
                "userId=" + userId +
                ", workId=" + workId +
                ", login='" + login + '\'' +
                ", fullName='" + fullName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", description='" + description + '\'' +
                ", statusWork='" + statusWork + '\'' +
                '}';
    }
}
