package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserDetailsDaoImpl;
import com.epam.tcfp.ave.database.dao.WorkDaoImpl;
import com.epam.tcfp.ave.database.dao.WorkHistoryDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import com.epam.tcfp.ave.entity.UserDetails;
import com.epam.tcfp.ave.entity.Work;
import com.epam.tcfp.ave.entity.WorkHistory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class AddHistoryService implements Service{
    WorkDaoImpl workDao = new WorkDaoImpl();
    WorkHistoryDaoImpl workHistoryDao = new WorkHistoryDaoImpl();
    UserDetailsDaoImpl userDetailsDao = new UserDetailsDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        HttpSession session = request.getSession(true);
        WorkHistory workHistory = new WorkHistory();
        UserAccount userAccount = (UserAccount)session.getAttribute(USER_ACCOUNT);
        Work work = workDao.getWorkById(Long.parseLong(request.getParameter(WORK_ID)));
        UserDetails userDetails = userDetailsDao.getUserDetailsByUserId(userAccount.getId());
        String fullName = userDetails.getFirstname()+ " " + userDetails.getLastname();
        workHistory.setUserId(userAccount.getId());
        workHistory.setWorkId(Long.parseLong(request.getParameter(WORK_ID)));
        workHistory.setLogin(userAccount.getLogin());
        workHistory.setFullName(fullName);
        workHistory.setCompanyName(work.getCompanyName());
        workHistory.setDescription(work.getDescription());
        workHistory.setStatusWork(APPLIED);
        workHistoryDao.addWorkHistory(workHistory);
        request.setAttribute(WORK_HISTORY, workHistory);
        dispatcher = request.getRequestDispatcher(INDEX_JSP);
        dispatcher.forward(request, response);
    }
}
