package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import com.epam.tcfp.ave.entity.Work;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class AddWorkService implements Service {
	WorkDaoImpl workDao = new WorkDaoImpl();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
		Work work = new Work();
		HttpSession session = request.getSession(true);
		UserAccount userAccount = (UserAccount)session.getAttribute(USER_ACCOUNT);
		Long user_id = userAccount.getId();

		work.setCompanyName(request.getParameter(COMPANY_NAME));
		work.setPhoneNumber(request.getParameter(PHONE_NUMBER));
		work.setCity(request.getParameter(CITY));
		work.setStreetAddress(request.getParameter(STREET_ADDRESS));
		work.setHomeNumber(request.getParameter(HOME_NUMBER));
		work.setApartmentNumber(request.getParameter(APARTMENT_NUMBER));
		work.setDescription(request.getParameter(DESCRIPTION));
		work.setPayment(request.getParameter(PAYMENT));
		work.setUserId(user_id);
		workDao.addWork(work);
		request.setAttribute(WORK, workDao.getLastWork());
		RequestDispatcher dispatcher = request.getRequestDispatcher(UPLOAD_PICTURE_JSP);
		dispatcher.forward(request, response);
	}
}
