package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.LanguageDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class ChangeLanguageService implements Service {
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        HttpSession session = request.getSession(true);
        LanguageDaoImpl languageDao = new LanguageDaoImpl();

        String languageToChange = request.getParameter(LANGUAGE);
        Integer languageID = languageDao.getLanguageId(languageToChange);

        if (languageID.equals(ENGLISH_ID) && languageToChange.equalsIgnoreCase(ENGLISH)) {
            session.setAttribute(LANGUAGE_ID, ENGLISH_ID);
            session.setAttribute(LANGUAGE, ENGLISH);
        } else if (languageID.equals(RUSSIAN_ID) && languageToChange.equalsIgnoreCase(RUSSIAN)) {
            session.setAttribute(LANGUAGE_ID, RUSSIAN_ID);
            session.setAttribute(LANGUAGE, RUSSIAN);
        }
        dispatcher = request.getRequestDispatcher(INDEX_JSP);
        dispatcher.forward(request, response);
    }
}
