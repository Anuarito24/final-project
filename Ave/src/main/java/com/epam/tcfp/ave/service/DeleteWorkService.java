package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkDaoImpl;
import com.epam.tcfp.ave.entity.Work;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.WORK_ID;
import static com.epam.tcfp.ave.util.constants.VariableNamesConst.WORK_LIST;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.UPLOAD_WORKS_JSP;

public class DeleteWorkService implements Service {
    WorkDaoImpl workDao = new WorkDaoImpl();

    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        long id = Long.parseLong(request.getParameter(WORK_ID));
        List<Work> workList;

        workDao.delete(id);
        workList = workDao.getAll();
        request.setAttribute(WORK_LIST, workList);
        dispatcher = request.getRequestDispatcher(UPLOAD_WORKS_JSP);
        dispatcher.forward(request, response);
    }
}
