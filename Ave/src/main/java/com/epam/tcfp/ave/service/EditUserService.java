package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserDetailsDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import com.epam.tcfp.ave.entity.UserDetails;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.ErrorsConst.*;
import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;
import static com.epam.tcfp.ave.util.validator.RegistrationValidator.validatePasswordRegex;
import static com.epam.tcfp.ave.util.validator.RegistrationValidator.validatePhoneNumber;

public class EditUserService implements Service {
    UserDetailsDaoImpl userDetailsDao = new UserDetailsDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        HttpSession session = request.getSession(true);

        UserAccount userAccount = (UserAccount)session.getAttribute(USER_ACCOUNT);
        if (!validatePasswordRegex(request.getParameter(PASSWORD))) {
            request.setAttribute(WRONG_PASSWORD, INVALID_PASSWORD);
            dispatcher = request.getRequestDispatcher(EDIT_USER_JSP);
            dispatcher.forward(request, response);
        } else if (!request.getParameter(PASSWORD).equals(request.getParameter(CONFIRM_PASSWORD))) {
            request.setAttribute(WRONG_PASSWORD_NOT_EQUAL, PASSWORD_NOT_EQUAL);
            dispatcher = request.getRequestDispatcher(EDIT_USER_JSP);
            dispatcher.forward(request, response);
        } else {
            userAccount.setPassword(DigestUtils.md5Hex(request.getParameter(PASSWORD)));
        }
        UserDetails userDetails = userDetailsDao.getUserDetailsByUserId(userAccount.getId());
        userDetails.setFirstname(request.getParameter(FIRSTNAME));
        userDetails.setLastname(request.getParameter(LASTNAME));
        userDetails.setGender(request.getParameter(GENDER));
        userDetails.setPhone(request.getParameter(PHONE));
        userDetails.setOccupation(request.getParameter(OCCUPATION));
        userDetails.setWorkExperience(request.getParameter(WORK_EXPERIENCE));
        userDetails.setUserId(userAccount.getId());
        if (!validatePhoneNumber(request.getParameter(PHONE))) {
            request.setAttribute(WRONG_PHONE, INVALID_PHONE);
            dispatcher = request.getRequestDispatcher(EDIT_USER_JSP);
            dispatcher.forward(request, response);
        } else if (userDetails.getId() == 0) {
            userDetailsDao.addUserDetails(userDetails);
        } else {
            userDetailsDao.update(userDetails);
        }
        request.setAttribute(SAVING_SUCCESS, DETAILS_SAVED);
        session.setAttribute(USER_DETAILS, userDetails);
        dispatcher = request.getRequestDispatcher(EDIT_USER_JSP);
        dispatcher.forward(request, response);
    }
}
