package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserAccountDaoImpl;
import com.epam.tcfp.ave.database.dao.UserDetailsDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import com.epam.tcfp.ave.entity.UserDetails;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class EditUsersAccountsService implements Service{
    UserAccountDaoImpl userAccountDao = new UserAccountDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        long id = Long.parseLong(request.getParameter(USER_ACCOUNT_ID));
        String edit = request.getParameter(EDIT);
        if (edit != null && edit.equals(EDIT)) {
            UserAccount userAccount = userAccountDao.getUserAccountById(id);
            userAccount.setRole(request.getParameter(ROLE));
            userAccount.setBanned(Boolean.parseBoolean(request.getParameter(BAN)));
            userAccountDao.update(userAccount);
        } else {
            userAccountDao.delete(id);
        }
        List<UserAccount> userAccountList = userAccountDao.getAll();
        request.setAttribute(USER_ACCOUNT_LIST, userAccountList);
        dispatcher = request.getRequestDispatcher(EDIT_USER_ACCOUNTS_JSP);
        dispatcher.forward(request, response);
    }
}
