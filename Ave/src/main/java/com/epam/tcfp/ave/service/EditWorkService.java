package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkDaoImpl;
import com.epam.tcfp.ave.entity.Work;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class EditWorkService implements Service{
    WorkDaoImpl workDao = new WorkDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        long id = Long.parseLong(request.getParameter(WORK_ID));
        Work work = workDao.getWorkById(id);
        List<Work> workList;

        work.setCompanyName(request.getParameter(COMPANY_NAME));
        work.setPhoneNumber(request.getParameter(PHONE_NUMBER));
        work.setCity(request.getParameter(CITY));
        work.setStreetAddress(request.getParameter(STREET_ADDRESS));
        work.setHomeNumber(request.getParameter(HOME_NUMBER));
        work.setApartmentNumber(request.getParameter(APARTMENT_NUMBER));
        work.setDescription(request.getParameter(DESCRIPTION));
        work.setPayment(request.getParameter(PAYMENT));
        workDao.update(work);
        workList = workDao.getAll();
        request.setAttribute(WORK_LIST, workList);
        dispatcher = request.getRequestDispatcher(UPLOAD_WORKS_JSP);
        dispatcher.forward(request, response);
    }
}
