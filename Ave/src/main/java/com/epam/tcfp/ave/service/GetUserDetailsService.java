package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserDetailsDaoImpl;
import com.epam.tcfp.ave.entity.UserDetails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;


public class GetUserDetailsService implements Service {
    UserDetailsDaoImpl userDetailsDao = new UserDetailsDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        HttpSession session = request.getSession(true);
        UserDetails userDetails = userDetailsDao.getUserDetailsByUserId((long)session.getAttribute("id"));

        session.setAttribute(USER_DETAILS, userDetails);
        response.sendRedirect(EDIT_USER_ACCOUNTS_JSP);
    }
}
