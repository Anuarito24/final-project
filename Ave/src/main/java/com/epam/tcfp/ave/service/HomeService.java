package com.epam.tcfp.ave.service;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;


public class HomeService implements Service{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
		RequestDispatcher dispatcher = request.getRequestDispatcher(INDEX_JSP);
		dispatcher.forward(request, response);
	}
}
