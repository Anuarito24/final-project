package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserAccountDaoImpl;
import com.epam.tcfp.ave.database.dao.UserDetailsDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import com.epam.tcfp.ave.entity.UserDetails;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.ErrorsConst.*;
import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class LoginService implements Service{
	UserAccountDaoImpl userAccountDao = new UserAccountDaoImpl();
	UserDetailsDaoImpl userDetailsDao = new UserDetailsDaoImpl();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
		HttpSession session = request.getSession(true);
		RequestDispatcher dispatcher;
		String login = request.getParameter(LOGIN);
		String password = request.getParameter(PASSWORD);
		String cipherPassword = DigestUtils.md5Hex(password);
		UserAccount userAccount = userAccountDao.getUserAccountByLoginPassword(login, cipherPassword);
		UserDetails userDetails = userDetailsDao.getUserDetailsByUserId(userAccount.getId());

		if (userAccount.getId() != 0) {
			if (!userAccount.getBanned()) {
				session.setAttribute(USER_ACCOUNT, userAccount);
				if (userDetails.getId() != 0) {
					session.setAttribute(USER_DETAILS, userDetails);
				}
				dispatcher = request.getRequestDispatcher(WELCOME_JSP);
				dispatcher.forward(request, response);
			}
			else {
				request.setAttribute(BANNED_LOGIN, LOGIN_IS_BANNED);
				dispatcher = request.getRequestDispatcher(LOGIN_JSP);
				dispatcher.forward(request, response);
			}
		} else {
			request.setAttribute(WRONG_LOGIN_OR_PASSWORD, MISTAKE_IN_LOGIN_PASSWORD);
			dispatcher = request.getRequestDispatcher(LOGIN_JSP);
			dispatcher.forward(request, response);
		}
	}
}
