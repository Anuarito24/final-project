package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserAccountDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;


import static com.epam.tcfp.ave.util.constants.ErrorsConst.*;
import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;
import static com.epam.tcfp.ave.util.validator.RegistrationValidator.*;

public class RegistrationService implements Service{
	UserAccountDaoImpl userAccountDao = new UserAccountDaoImpl();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
		RequestDispatcher dispatcher;
		UserAccount userAccount = new UserAccount();

		userAccount.setLogin(request.getParameter(LOGIN));
		userAccount.setEmail(request.getParameter(EMAIL));
		userAccount.setPassword(DigestUtils.md5Hex(request.getParameter(PASSWORD)));
		userAccount.setRole(EMPLOYEE);
		userAccount.setBanned(false);
		if (userAccountDao.isLoginExist(request.getParameter(LOGIN))) {
			request.setAttribute(WRONG_LOGIN, LOGIN_EXISTS_ERROR);
			dispatcher = request.getRequestDispatcher(REGISTRATION_JSP);
			dispatcher.forward(request, response);
		} else if (userAccountDao.isEmailExist(request.getParameter(EMAIL))) {
			request.setAttribute(EMAIL_EXIST, EMAIL_EXISTS_ERROR);
			dispatcher = request.getRequestDispatcher(REGISTRATION_JSP);
			dispatcher.forward(request, response);
		} else if (!validateMailRegex(request.getParameter(EMAIL))) {
			request.setAttribute(WRONG_EMAIL, EMAIL_IS_WRONG);
			dispatcher = request.getRequestDispatcher(REGISTRATION_JSP);
			dispatcher.forward(request, response);
		} else if (!validatePasswordRegex(request.getParameter(PASSWORD))) {
			request.setAttribute(WRONG_PASSWORD, INVALID_PASSWORD);
			dispatcher = request.getRequestDispatcher(REGISTRATION_JSP);
			dispatcher.forward(request, response);
		} else if (!request.getParameter(PASSWORD).equals(request.getParameter(CONFIRM_PASSWORD))) {
			request.setAttribute(WRONG_PASSWORD_NOT_EQUAL, PASSWORD_NOT_EQUAL);
			dispatcher = request.getRequestDispatcher(REGISTRATION_JSP);
			dispatcher.forward(request, response);
		} else {
			userAccountDao.addUserAccount(userAccount);
			request.setAttribute(REGISTRATION_SUCCESSFULLY, SUCCESS_REGISTRATION);
			dispatcher = request.getRequestDispatcher(REGISTRATION_JSP);
			dispatcher.forward(request, response);
		}
	}
}
