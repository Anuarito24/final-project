package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkDaoImpl;
import com.epam.tcfp.ave.entity.Work;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class ShowWorkService implements Service{
    WorkDaoImpl workDao = new WorkDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        long id = Long.parseLong(request.getParameter(WORK_ID));
        Work work = workDao.getWorkById(id);

        request.setAttribute(WORK, work);
        dispatcher = request.getRequestDispatcher(VIEW_WORK_JSP);
        dispatcher.forward(request, response);
    }
}
