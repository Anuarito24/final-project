package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkDaoImpl;
import com.epam.tcfp.ave.entity.Work;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;


import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class UploadPictureService implements Service{
    static final int fileMaxSize = 1024 * 1024 * 50;
    static final int memMaxSize = 1024 * 1024 * 50;
    WorkDaoImpl workDao = new WorkDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        String workId = request.getParameter("work_id");
        DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
        String filePath = "D:\\Ave\\uploads\\";
        diskFileItemFactory.setRepository(new File(filePath));
        diskFileItemFactory.setSizeThreshold(memMaxSize);

        Work work = workDao.getWorkById(Long.parseLong(workId));
        ServletFileUpload upload = new ServletFileUpload(diskFileItemFactory);
        upload.setSizeMax(fileMaxSize);

        try {
            List fileItems = upload.parseRequest(request);
            Iterator iterator = fileItems.iterator();

            while (iterator.hasNext()) {
                FileItem fileItem = (FileItem) iterator.next();
                if (!fileItem.isFormField()) {
                    String fileName = fileItem.getName();
                    File file;
                    if (fileName.lastIndexOf("\\") >= 0) {
                        file = new File(filePath +
                                fileName.substring(fileName.lastIndexOf("\\")));
                    } else {
                        file = new File(filePath +
                                fileName.substring(fileName.lastIndexOf("\\") + 1));
                    }
                    work.setPictureName(fileName);
                    workDao.update(work);
                    fileItem.write(file);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(ADD_WORK_JSP);
        dispatcher.forward(request, response);
    }
}
