package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkHistoryDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;
import com.epam.tcfp.ave.entity.WorkHistory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.USER_ACCOUNT;
import static com.epam.tcfp.ave.util.constants.VariableNamesConst.WORK_HISTORY_LIST;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.CONFIRM_WORK_JSP;

public class UploadUserWorkService implements Service{
    WorkHistoryDaoImpl workHistoryDao = new WorkHistoryDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        HttpSession session = request.getSession(true);
        UserAccount userAccount = (UserAccount)session.getAttribute(USER_ACCOUNT);
        List<WorkHistory> workHistoryList = workHistoryDao.getAllWorkHistoryByUserId(userAccount.getId());

        request.setAttribute(WORK_HISTORY_LIST, workHistoryList);
        dispatcher = request.getRequestDispatcher(CONFIRM_WORK_JSP);
        dispatcher.forward(request, response);
    }
}
