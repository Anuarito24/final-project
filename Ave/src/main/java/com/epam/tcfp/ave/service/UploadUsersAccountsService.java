package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.UserAccountDaoImpl;
import com.epam.tcfp.ave.entity.UserAccount;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.USER_ACCOUNT_LIST;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.EDIT_USER_ACCOUNTS_JSP;

public class UploadUsersAccountsService implements Service {
    UserAccountDaoImpl userAccountDao = new UserAccountDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        List<UserAccount> userAccountList = userAccountDao.getAll();

        request.setAttribute(USER_ACCOUNT_LIST, userAccountList);
        dispatcher = request.getRequestDispatcher(EDIT_USER_ACCOUNTS_JSP);
        dispatcher.forward(request, response);
    }
}
