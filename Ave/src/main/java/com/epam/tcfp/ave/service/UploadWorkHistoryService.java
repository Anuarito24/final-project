package com.epam.tcfp.ave.service;

import com.epam.tcfp.ave.database.dao.WorkHistoryDaoImpl;
import com.epam.tcfp.ave.entity.WorkHistory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.epam.tcfp.ave.util.constants.VariableNamesConst.*;
import static com.epam.tcfp.ave.util.constants.WebPageNamesConst.*;

public class UploadWorkHistoryService implements Service {
    WorkHistoryDaoImpl workHistoryDao = new WorkHistoryDaoImpl();
    RequestDispatcher dispatcher;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        List<WorkHistory> workHistoryList = workHistoryDao.getAll();

        request.setAttribute(WORK_HISTORY_LIST, workHistoryList);
        dispatcher = request.getRequestDispatcher(CONFIRM_WORK_JSP);
        dispatcher.forward(request, response);
    }
}
