package com.epam.tcfp.ave.service.factory;

import com.epam.tcfp.ave.service.*;

import java.util.HashMap;
import java.util.Map;

public class ServiceFactory {
	private static final Map<String, Service> SERVICE_MAP = new HashMap<>();
	private static final ServiceFactory SERVICE_FACTORY = new ServiceFactory();

	private ServiceFactory() {
	}

	static {
		SERVICE_MAP.put("/SRM", new HomeService());
		SERVICE_MAP.put("/LOGIN", new LoginService());
		SERVICE_MAP.put("/LOGOUT", new LogoutService());
		SERVICE_MAP.put("/EDIT_USER", new EditUserService());
		SERVICE_MAP.put("/EDIT_WORK", new EditWorkService());
		SERVICE_MAP.put("/REGISTRATION", new RegistrationService());
		SERVICE_MAP.put("/WELCOME", new WelcomeService());
		SERVICE_MAP.put("/ADD_WORK", new AddWorkService());
		SERVICE_MAP.put("/EDIT_USERS_ACCOUNTS", new EditUsersAccountsService());
		SERVICE_MAP.put("/UPLOAD_USERS_ACCOUNTS", new UploadUsersAccountsService());
		SERVICE_MAP.put("/UPLOAD_WORKS", new UploadWorksService());
		SERVICE_MAP.put("/FILL_WORK_FIELDS", new FillWorkFieldsService());
		SERVICE_MAP.put("/SHOW_WORK", new ShowWorkService());
		SERVICE_MAP.put("/ADD_HISTORY", new AddHistoryService());
		SERVICE_MAP.put("/UPLOAD_WORK_HISTORY", new UploadWorkHistoryService());
		SERVICE_MAP.put("/CONFIRM_WORK", new ConfirmWorkService());
		SERVICE_MAP.put("/UPLOAD_USER_WORK", new UploadUserWorkService());
		SERVICE_MAP.put("/CHANGE_LANGUAGE", new ChangeLanguageService());
		SERVICE_MAP.put("/UPLOAD_PICTURE", new UploadPictureService());
		SERVICE_MAP.put("/DELETE", new DeleteWorkService());
	}

	public Service getService(String request) {
		Service service = SERVICE_MAP.get("/ERROR");

		for (Map.Entry<String, Service> pair : SERVICE_MAP.entrySet()) {
			if (request.equalsIgnoreCase(pair.getKey())) {
				service = SERVICE_MAP.get(pair.getKey());
			}
		}
		return service;
	}
	public static ServiceFactory getInstance() {
		return SERVICE_FACTORY;
	}
}
