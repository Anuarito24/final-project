package com.epam.tcfp.ave.util.constants;

public class ErrorsConst {
    public static final String WRONG_LOGIN = "loginIsExist";
    public static final String LOGIN_EXISTS_ERROR = "login.exists";

    public static final String BANNED_LOGIN = "loginIsExist";
    public static final String LOGIN_IS_BANNED = "login.banned";

    public static final String WRONG_EMAIL = "emailIsWrong";
    public static final String EMAIL_IS_WRONG = "register.email.incorrect";

    public static final String EMAIL_EXIST = "emailIsExist";
    public static final String EMAIL_EXISTS_ERROR = "register.email.already.using";

    public static final String WRONG_PASSWORD = "invalidPassword";
    public static final String INVALID_PASSWORD = "invalid.password";

    public static final String WRONG_PASSWORD_NOT_EQUAL = "notEqualPassword";
    public static final String PASSWORD_NOT_EQUAL = "not.equal.password";

    public static final String WRONG_LOGIN_OR_PASSWORD = "wrongLoginOrPassword";
    public static final String MISTAKE_IN_LOGIN_PASSWORD = "wrong.login.or.password";

    public static final String WRONG_PHONE = "invalidPhone";
    public static final String INVALID_PHONE = "invalid.phone";

    public static final String SAVING_SUCCESS = "savingIsSuccess";
    public static final String DETAILS_SAVED = "saved.successfully";


    public static final String REGISTRATION_SUCCESSFULLY = "registerIsSuccess";
    public static final String SUCCESS_REGISTRATION = "registration.successfully";
}
