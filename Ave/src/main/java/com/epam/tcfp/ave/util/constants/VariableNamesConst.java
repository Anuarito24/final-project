package com.epam.tcfp.ave.util.constants;

public class VariableNamesConst {

    public static final String ENGLISH = "en_US";
    public static final String RUSSIAN = "ru_RU";
    public static final Integer ENGLISH_ID = 1;
    public static final Integer RUSSIAN_ID = 2;


    public Integer getEnglishId() {
        return ENGLISH_ID;
    }

    public Integer getRussianId() {
        return RUSSIAN_ID;
    }

    public static final String ID = "id";
    public static final String LANGUAGE = "language";
    public static final String LANGUAGES = "languages";
    public static final String LANGUAGE_ID = "languageID";
    public static final String LOGIN = "login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String CONFIRM_PASSWORD = "confirm";
    public static final String CREATE_DATE = "createDate";
    public static final String ROLE = "role";
    public static final String BAN = "ban";
    public static final String BANNED = "banned";
    public static final String USER_ACCOUNT = "userAccount";
    public static final String USER_ACCOUNT_ID = "userAccountId";
    public static final String USER_ACCOUNT_LIST = "userAccountList";
    public static final String WORK = "work";
    public static final String WORK_ID = "work_id";
    public static final String WORK_LIST = "workList";
    public static final String WORK_HISTORY = "work_history";
    public static final String WORK_HISTORY_LIST = "workHistoryList";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String GENDER = "gender";
    public static final String PHONE = "phone";
    public static final String OCCUPATION = "occupation";
    public static final String WORK_EXPERIENCE = "workExperience";
    public static final String USER_ID = "user_id";
    public static final String USER_DETAILS = "userDetails";
    public static final String COMPANY_NAME = "company_name";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String CITY = "city";
    public static final String STREET_ADDRESS = "street_address";
    public static final String HOME_NUMBER = "home_number";
    public static final String APARTMENT_NUMBER = "apartment_number";
    public static final String DESCRIPTION = "description";
    public static final String PAYMENT = "payment";
    public static final String APPLIED = "applied";
    public static final String STATUS_WORK = "status_work";
    public static final String EDIT = "edit";
    public static final String EMPLOYEE = "Employee";
    public static final String PICTURE_NAME = "picture_name";
    public static final String FULL_NAME = "full_name";
}
