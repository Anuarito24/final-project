package com.epam.tcfp.ave.util.constants;

public class WebPageNamesConst {
    public static final String ADD_WORK_JSP = "/add_work.jsp";
    public static final String EDIT_USER_JSP = "/edit_user.jsp";
    public static final String EDIT_WORK_JSP = "/edit_work.jsp";
    public static final String ERROR_JSP = "/error.jsp";
    public static final String FOOTER_JSP = "/footer.jsp";
    public static final String HEADER_JSP = "/header.jsp";
    public static final String INDEX_JSP = "/index.jsp";
    public static final String LOGIN_JSP = "/login.jsp";
    public static final String LOGOUT_JSP = "/logout.jsp";
    public static final String REGISTRATION_JSP = "/registration.jsp";
    public static final String UPLOAD_WORKS_JSP = "/upload_works.jsp";
    public static final String VIEW_WORK_JSP = "/view_work.jsp";
    public static final String WELCOME_JSP = "/welcome.jsp";
    public static final String EDIT_USER_ACCOUNTS_JSP = "/edit_users_accounts.jsp";
    public static final String CONFIRM_WORK_JSP = "/confirm_work.jsp";
    public static final String UPLOAD_JSP = "/upload.jsp";
    public static final String UPLOAD_PICTURE_JSP = "/upload_picture.jsp";
}
