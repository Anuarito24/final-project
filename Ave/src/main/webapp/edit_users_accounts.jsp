<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 16.01.2021
  Time: 1:38
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.edit.users.accounts"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">

    <!-- Bootstrap core CSS -->
    <style>
        <%@include file= 'assets/dist/css/bootstrap.min.css'%>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <style>
        <%@include file= 'css/signin.css'%>
        <%@include file= 'css/sticky-footer-navbar.css'%>
    </style>

</head>
<body class="d-flex flex-column h-100">
    <jsp:include page="header.jsp"/>
    <main class="form-signin">
        <form class="form-horizontal" method="post" action="edit_users_accounts">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col"><fmt:message key="edit.users.accounts.login"/></th>
                        <th scope="col"><fmt:message key="edit.users.accounts.email"/></th>
                        <th scope="col"><fmt:message key="edit.users.accounts.role"/></th>
    <%--                    <th scope="col">Firstname</th>--%>
    <%--                    <th scope="col">Lastname</th>--%>
    <%--                    <th scope="col">Gender</th>--%>
    <%--                    <th scope="col">Phone</th>--%>
    <%--                    <th scope="col">Occupation</th>--%>
    <%--                    <th scope="col">Work experience</th>--%>
                        <th scope="col"><fmt:message key="edit.users.accounts.ban"/></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="userAccount" items="${requestScope.userAccountList}">
                    <tr>
<%--                        <th scope="row">1</th>--%>
                        <td>
                            <input type="hidden" name="userAccountId" value="${userAccount.id}">
                        </td>
                        <td>${userAccount.login}</td>
                        <td>${userAccount.email}</td>
                        <td>
                            <select class="custom-select-sm" name="role">
                                <option <c:if test="${userAccount.role eq 'Employee'}"> selected</c:if>>Employee</option>
                                <option <c:if test="${userAccount.role eq 'Admin'}"> selected</c:if>>Admin</option>
                            </select>
                        </td>
                        <td>
                            <select class="custom-select-sm" name="ban">
                                <option <c:if test="${userAccount.banned eq 'false'}"> selected</c:if>>false</option>
                                <option <c:if test="${userAccount.banned eq 'true'}"> selected</c:if>>true</option>
                            </select>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-success" name="edit" value="edit"><fmt:message key="edit.users.accounts.edit"/></button>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-danger" name="delete" value="delete"><fmt:message key="edit.users.accounts.delete"/></button>
                        </td>
                    </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
    </main>
    <jsp:include page="footer.jsp"/>
    <script src="assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
