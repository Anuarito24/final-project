<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 15.01.2021
  Time: 16:51
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.error"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">
    <style>
        <%@include file= 'css/sticky-footer-navbar.css'%>
    </style>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <h4><fmt:message key="error.message"/></h4>
    <jsp:include page="footer.jsp"/>
</body>
</html>
