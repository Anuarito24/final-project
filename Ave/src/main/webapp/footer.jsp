<%--
  Created by IntelliJ IDEA.
  User: Anuar
  Date: 08.01.2021
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<footer class="footer mt-auto py-3 bg-light">
  <div class="container">
    <span class="text-muted">&copy; 2021</span>
  </div>
</footer>
