<%--
  Created by IntelliJ IDEA.
  User: Anuar
  Date: 02.01.2021
  Time: 22:49
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.sign.in"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <style>
        <%@include file='assets/dist/css/bootstrap.min.css'%>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <style>
        <%@include file='css/signin.css' %>
    </style>
</head>
    <body class="text-center">
        <jsp:include page="header.jsp"/>
        <main class="form-signin">
            <form  method="post" action="login">
                <img width="70%" src="images/Ave.svg.png"/>
                <br/>
                <label for="inputLogin" class="visually-hidden"><fmt:message key="edit.user.login"/></label>
                <input type="text"  name="login" id="inputLogin" class="form-control" placeholder="<fmt:message key="edit.user.login"/>" required autofocus>
                <label for="inputPassword" class="visually-hidden"><fmt:message key="login.password"/></label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="<fmt:message key="login.password"/>" required>
                <c:if test="${requestScope.wrongLoginOrPassword != null}">
                    <small class="text-danger"><fmt:message key="error.wrong.login.or.password"/></small>
                </c:if>
                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" value="remember-me"> <fmt:message key="login.remember.me"/>
                    </label>
                </div>
                <button class="w-100 btn btn-lg btn-primary" type="submit"><fmt:message key="header.login"/></button>
            <c:if test="${requestScope.loginIsExist != null}">
                <small class="text-danger"><fmt:message key="error.login.banned"/></small>
            </c:if>
            </form>
        </main>
        <jsp:include page="footer.jsp"/>
        <script src="assets/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
