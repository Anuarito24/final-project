<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 19.01.2021
  Time: 11:19
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.view.application"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">

    <!-- Bootstrap core CSS -->
    <style>
        <%@include file= 'assets/dist/css/bootstrap.min.css'%>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <style>
        <%@include file='css/signin.css' %>
        <%@include file='css/cheatsheet.css'%>
        <%@include file= 'css/sticky-footer-navbar.css'%>
    </style>

</head>
<body class="text-center">
    <jsp:include page="header.jsp"/>
        <div class="col">
            <div class="card">
<%--                <c:choose>--%>
<%--                    <c:when test="${requestScope.work.pictureName ne null}">--%>
<%--                        <img class="bd-placeholder-img card-img-top" src="${pageContext.request.contextPath}D:/Ave/uploads/${requestScope.work.pictureName}" alt="logo"/>--%>
<%--                    </c:when>--%>
<%--                    <c:otherwise>--%>
<%--                        <img class="bd-placeholder-img" width="50%" src="${pageContext.request.contextPath}D:/Ave/uploads/notExist.png" alt="logo"/>--%>
<%--                    </c:otherwise>--%>
<%--                </c:choose>--%>
                <img class="list-group list-group-flush" width="50%" src="controller/ImageShowServlet?work_pictureName=${requestScope.work.pictureName}" alt=""/>

                <div class="card-body">
                    <h5 class="card-title"><fmt:message key="view.work.overview"/></h5>
                    <p class="card-text" name="description">${requestScope.work.description}</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item" name="company_name"><fmt:message key="view.work.company.name"/>: ${requestScope.work.companyName}</li>
                    <li class="list-group-item"><fmt:message key="view.work.contact.number"/>: ${requestScope.work.phoneNumber}</li>
                    <li class="list-group-item"><fmt:message key="view.work.city"/>: ${requestScope.work.city}</li>
                    <li class="list-group-item"><fmt:message key="view.work.address"/>: ${requestScope.work.streetAddress},
                        ${requestScope.work.homeNumber}, <fmt:message key="view.work.apt"/> ${requestScope.work.apartmentNumber}</li>
                    <li class="list-group-item"><fmt:message key="view.work.payment"/>: ${requestScope.work.payment}</li>
                </ul>
                <div class="card-body">
                    <form action="add_history" method="post">
                        <button type="submit" class="btn btn-sm btn-outline-secondary" name="work_id"
                                value="${requestScope.work.id}"><fmt:message key="view.work.apply"/></button>
                    </form>
                </div>
            </div>
        </div>
</body>
<br/>
<jsp:include page="footer.jsp"/>
</html>
