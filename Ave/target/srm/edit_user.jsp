<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 11.01.2021
  Time: 21:33
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title><fmt:message key="title.edit.user"/></title>
        <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
        <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">
        <style>
            <%@include file= 'css/sticky-footer-navbar.css'%>
        </style>
    </head>

<!-- Bootstrap core CSS -->
<style>
    <%@include file='assets/dist/css/bootstrap.min.css' %>
</style>

<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>

<body>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<jsp:include page="header.jsp"/>
<main class="flex-shrink-">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><fmt:message key="edit.user"/></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="edit_user">
                            <div class="form-group">
                                <label for="login" class="cols-sm-2 control-label"><fmt:message key="edit.user.login"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="login" id="login"
                                               placeholder="Enter your login" value="${sessionScope.userAccount.login}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.email"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="email" id="email"
                                               placeholder="Enter your Email" value="${sessionScope.userAccount.email}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label"><fmt:message key="edit.user.password"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="password" id="password"
                                               placeholder="<fmt:message key="edit.user.enter.your.password"/>"/>
                                    </div>
                                </div>
                                <c:if test="${requestScope.invalidPassword != null}">
                                    <small class="text-danger"><fmt:message key="error.invalid.password"/></small>
                                </c:if>
                                <c:if test="${requestScope.notEqualPassword != null}">
                                    <small class="text-danger"><fmt:message key="error.not.equal.password"/></small>
                                </c:if>
                            </div>
                            <div class="form-group">
                                <label for="confirm" class="cols-sm-2 control-label"><fmt:message key="edit.user.confirm.password"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="confirm" id="confirm"
                                               placeholder="<fmt:message key="edit.user.confirm.your.password"/>" />
                                    </div>
                                </div>
                                <c:if test="${requestScope.invalidPassword != null}">
                                    <small class="text-danger"><fmt:message key="error.invalid.password"/></small>
                                </c:if>
                                <c:if test="${requestScope.notEqualPassword != null}">
                                    <small class="text-danger"><fmt:message key="error.not.equal.password"/></small>
                                </c:if>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.firstname"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="firstname" id="firstname"
                                               placeholder="<fmt:message key="edit.user.enter.firstname"/>" value="${sessionScope.userDetails.firstname}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.lastname"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="lastname" id="lastname"
                                               placeholder="<fmt:message key="edit.user.enter.lastname"/>" value="${sessionScope.userDetails.lastname}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gender" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.gender"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="gender" id="gender"
                                               placeholder="<fmt:message key="edit.user.enter.gender"/>" value="${sessionScope.userDetails.gender}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.phone"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="phone" id="phone"
                                               placeholder="<fmt:message key="edit.user.enter.phone"/>" value="${sessionScope.userDetails.phone}" required/>
                                        <p>Example: +X-XXX-XXXXXXX</p>
                                    </div>
                                </div>
                                <c:if test="${requestScope.invalidPhone != null}">
                                    <small class="text-danger"><fmt:message key="error.not.invalid.phone"/></small>
                                </c:if>
                            </div>
                            <div class="form-group">
                                <label for="occupation" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.occupation"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="occupation" id="occupation"
                                               placeholder="<fmt:message key="edit.user.enter.occupation"/>" value="${sessionScope.userDetails.occupation}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="work_experience" class="cols-sm-2 control-label"><fmt:message key="edit.user.your.work.experience"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="workExperience" id="work_experience"
                                               placeholder="<fmt:message key="edit.user.enter.work.experience"/>" value="${sessionScope.userDetails.workExperience}" required/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button"><fmt:message key="edit.user.button"/></button>
                            </div>
                            <c:if test="${requestScope.savingIsSuccess != null}">
                                <small class="text-danger"><fmt:message key="error.saved.successfully"/></small>
                            </c:if>
                            <div class="login-register">
                                <a href="/"><fmt:message key="add.work.go.back"/></a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
<jsp:include page="footer.jsp"/>

<script src="assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
