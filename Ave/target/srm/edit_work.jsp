<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 12.01.2021
  Time: 17:43
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.edit.application"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">
    <style>
        <%@include file= 'css/sticky-footer-navbar.css'%>
    </style>
</head>

<!-- Bootstrap core CSS -->
<style>
    <%@include file='assets/dist/css/bootstrap.min.css' %>
</style>

<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>

<body>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<jsp:include page="header.jsp"/>
<main class="flex-shrink-">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><fmt:message key="edit.work"/></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="edit_work">
                            <div class="form-group">
                                <label for="company_name" class="cols-sm-2 control-label"><fmt:message key="edit.work.company.name"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="company_name" id="company_name"
                                               placeholder="<fmt:message key="edit.work.enter.company.name"/>" value="${requestScope.work.companyName}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone_number" class="cols-sm-2 control-label"><fmt:message key="edit.work.phone.number"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="phone_number" id="phone_number"
                                               placeholder="<fmt:message key="edit.work.enter.phone.number"/>" value="${requestScope.work.phoneNumber}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="city" class="cols-sm-2 control-label"><fmt:message key="edit.work.city"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="city" id="city"
                                               placeholder="<fmt:message key="edit.work.enter.city"/>" value="${requestScope.work.city}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="street_address" class="cols-sm-2 control-label"><fmt:message key="edit.work.street.address"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="street_address" id="street_address"
                                               placeholder="<fmt:message key="edit.work.enter.street.address"/>" value="${requestScope.work.streetAddress}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="home_number" class="cols-sm-2 control-label"><fmt:message key="edit.work.home.number"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="number" class="form-control" name="home_number" id="home_number"
                                               placeholder="<fmt:message key="edit.work.enter.home.number"/>" value="${requestScope.work.homeNumber}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="apartment_number" class="cols-sm-2 control-label"><fmt:message key="edit.work.apartment.number"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="number" class="form-control" name="apartment_number" id="apartment_number"
                                               placeholder="<fmt:message key="edit.work.enter.apartment.number"/>" value="${requestScope.work.apartmentNumber}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="cols-sm-2 control-label"><fmt:message key="edit.work.description"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="description" id="description"
                                               placeholder="<fmt:message key="edit.work.enter.description"/>" value="${requestScope.work.description}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="payment" class="cols-sm-2 control-label"><fmt:message key="edit.work.payment"/></label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="payment" id="payment"
                                               placeholder="<fmt:message key="edit.work.enter.payment"/>" value="${requestScope.work.payment}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button"
                                        name="work_id" value="${requestScope.work.id}"><fmt:message key="edit.work"/></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
<jsp:include page="footer.jsp"/>

<script src="assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
