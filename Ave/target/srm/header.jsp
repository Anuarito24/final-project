<%--
  Created by IntelliJ IDEA.
  User: Anuar
  Date: 08.01.2021
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>


<header>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/navbars/">
    <style>
        <%@include file='assets/dist/css/bootstrap.min.css'%>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <style>
        <%@include file='css/navbar.css'%>
        <%@include file='css/cheatsheet.css'%>
    </style>
    <!-- Fixed navbar -->
    <div class="container">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.jsp">Ave</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleXxl">
                <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                    <li class="nav-item active">
                        <form class="form-horizontal" method="post" action="upload_works">
                            <button type="submit" class="btn btn-primary"><fmt:message key="header.show.available.works"/></button>
                      </form>
                    </li>
                    <li class="nav-item active">
                        <c:if test="${sessionScope.userAccount.role eq null}">
                            <li class="nav-item">
                                <a class="nav-link" href="login.jsp"><fmt:message key="header.login"/></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="registration.jsp"><fmt:message key="header.sign.up"/></a>
                            </li>
                        </c:if>
                    </li>
                    <li class="nav-item dropdown">
                    <c:if test="${sessionScope.userAccount.role ne null}">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">${sessionScope.userAccount.login}</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="visually-hidden">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <c:if test="${sessionScope.userAccount.role eq 'Admin'}">
                                    <li><a class="dropdown-item" href="add_work.jsp"><fmt:message key="header.add.work"/></a></li>
                                    <li>
                                        <form class="form-horizontal" method="post" action="upload_users_accounts">
                                            <button type="submit" class="dropdown-item"><fmt:message key="header.edit.users.work"/></button>
                                        </form>
                                    </li>
                                    <li>
                                        <form class="form-horizontal" method="post" action="upload_work_history">
                                            <button type="submit" class="dropdown-item"><fmt:message key="header.confirm.work"/></button>
                                        </form>
                                    </li>
                                </c:if>
                                <c:if test="${sessionScope.userAccount.role eq 'Employee'}">
                                    <li>
                                        <form class="form-horizontal" method="post" action="upload_user_work">
                                            <button type="submit" class="dropdown-item"><fmt:message key="header.history.work"/></button>
                                        </form>
                                    </li>
                                </c:if>
                                <li><a class="dropdown-item" href="edit_user.jsp"><fmt:message key="header.edit.user"/></a></li>
                                <li>
                                    <form class="form-horizontal" method="post" action="logout">
                                        <button type="submit" class="dropdown-item"><fmt:message key="header.logout"/></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                        </c:if>
                    </li>
                </ul>
                <form class="form-horizontal" action="change_language" method="get">
                    <input type="hidden" name="language" value="en_US">
                    <button type="submit" class="btn btn-dark"><fmt:message key="header.language.en"/></button>
                </form>
                <form class="form-horizontal" action="change_language" method="get">
                    <input type="hidden" name="language" value="ru_RU">
                    <button type="submit" class="btn btn-dark"><fmt:message key="header.language.ru"/></button>
                </form>
            </div>
        </div>
    </nav>
    </div>
    <script src="assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
</header>
