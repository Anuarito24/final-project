<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title.registration"/></title>
        <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    </head>
    <body class="text-center">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><fmt:message key="register"/></div>
                        <div class="card-body">
                            <form class="form-horizontal" method="post" action="registration">
                                <div class="form-group">
                                    <label for="login" class="cols-sm-2 control-label"><fmt:message key="register.your.login"/></label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="login" id="login" placeholder="<fmt:message key="register.enter.your.login"/>" />
                                        </div>
                                    </div>
                                    <c:if test="${requestScope.loginIsExist != null}">
                                        <small class="text-danger"><fmt:message key="register.login.already.exist"/></small>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="cols-sm-2 control-label"><fmt:message key="register.your.email"/></label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="email" id="email" placeholder="<fmt:message key="register.enter.your.email"/>"/>
                                        </div>
                                    </div>
                                    <c:if test="${requestScope.emailIsWrong != null}">
                                        <small class="text-danger"><fmt:message key="register.email.incorrect"/></small>
                                    </c:if>
                                    <c:if test="${requestScope.emailIsExist != null}">
                                        <small class="text-danger"><fmt:message key="register.email.already.using"/></small>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="cols-sm-2 control-label"><fmt:message key="register.password"/></label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" id="password" placeholder="<fmt:message key="register.enter.your.password"/>" />
                                        </div>
                                    </div>
                                    <c:if test="${requestScope.invalidPassword != null}">
                                        <small class="text-danger"><fmt:message key="register.password.invalid"/></small>
                                    </c:if>
                                    <c:if test="${requestScope.notEqualPassword != null}">
                                        <small class="text-danger"><fmt:message key="register.password.not.equal"/></small>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label"><fmt:message key="register.confirm.password"/></label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="confirm" id="confirm" placeholder="<fmt:message key="register.confirm.your.password"/>"/>
                                        </div>
                                    </div>
                                    <c:if test="${requestScope.invalidPassword != null}">
                                        <small class="text-danger"><fmt:message key="register.password.invalid"/></small>
                                    </c:if>
                                    <c:if test="${requestScope.notEqualPassword != null}">
                                        <small class="text-danger"><fmt:message key="register.password.not.equal"/></small>
                                    </c:if>
                                </div>
                                <div class="form-group ">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button"><fmt:message key="register"/></button>
                                </div>
                                <div class="login-register">
                                    <a href="login.jsp"><fmt:message key="register.login"/></a>
                                </div>
                                <c:if test="${requestScope.registerIsSuccess != null}">
                                    <small class="text-danger"><fmt:message key="register.success"/></small>
                                </c:if>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
