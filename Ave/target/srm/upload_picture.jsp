<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 27.01.2021
  Time: 1:19
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title><fmt:message key="title.upload.picture"/></title>
        <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
        <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">
        <style>
            <%@include file= 'css/sticky-footer-navbar.css'%>
        </style>
    </head>

    <!-- Bootstrap core CSS -->
    <style>
        <%@include file='assets/dist/css/bootstrap.min.css' %>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <body>
        <jsp:include page="header.jsp"/>
        <h1><fmt:message key="upload.file"/></h1>
        <h3><fmt:message key="upload.please.select.file"/></h3> <br/>
        <main class="flex-shrink-">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header"><fmt:message key="header.add.work"/></div>
                            <div class="card-body">
                                <form action="upload_picture?work_id=${requestScope.work.id}" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                            <div class="mb-3">
                                                <label class="form-label" for="customFile"><fmt:message key="add.work.upload.photo"/></label>
                                                <input type="file" class="form-control" id="customFile" name="upload_photo">
                                            </div>
                                            <input type="submit" value="Upload File"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
