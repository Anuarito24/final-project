<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 19.01.2021
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.upload.user.applications"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/">

    <!-- Bootstrap core CSS -->
    <style>
        <%@include file= 'assets/dist/css/bootstrap.min.css'%>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <style>
        <%@include file= 'css/signin.css'%>
        <%@include file= 'css/sticky-footer-navbar.css'%>
    </style>

</head>
<body class="d-flex flex-column h-100">
<jsp:include page="header.jsp"/>
<main class="form-signin">
    <form class="form-horizontal" method="post" action="confirm_work">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><fmt:message key="upload.login"/></th>
                <th scope="col"><fmt:message key="upload.full.name"/></th>
                <th scope="col"><fmt:message key="upload.company.name"/></th>
                <th scope="col"><fmt:message key="upload.description"/></th>
                <th scope="col"><fmt:message key="upload.status.of.work"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="work_history" items="${requestScope.workHistoryList}">
                <tr>
                    <td>
                        <input type="hidden" name="id" value="${work_history.id}">
                    </td>
                    <td>${work_history.login}</td>
                    <td>${work_history.fullName}</td>
                    <td>${work_history.companyName}</td>
                    <td>${work_history.description}</td>
                    <td>${work_history.statusWork}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form>
</main>
<jsp:include page="footer.jsp"/>
<script src="assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
