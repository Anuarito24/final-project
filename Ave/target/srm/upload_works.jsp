<%--
  Created by IntelliJ IDEA.
  User: Anuar.Aimagambetov
  Date: 12.01.2021
  Time: 11:29
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><fmt:message key="title.available.applications"/></title>
    <link rel="icon" href="images/Ave.svg.png" type="image/icon type">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">


    <!-- Bootstrap core CSS -->
    <style>
        <%@include file='assets/dist/css/bootstrap.min.css' %>
    </style>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <main>

            <section class="py-5 text-center container">
                <div class="row py-lg-5">
                    <div class="col-lg-6 col-md-8 mx-auto">
                        <h1 class="fw-light"><fmt:message key="upload.works.available.applications"/></h1>
                    </div>
                </div>
            </section>

            <div class="album py-5 bg-light">
                <div class="container">

                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                            <c:forEach var="work" items="${requestScope.workList}" >
                                <div class="col">
                                    <div class="card shadow-sm">
                                        <img class="bd-placeholder-img card-img-top" width="100%" height="225" src="controller/ImageShowServlet?work_pictureName=${work.pictureName}" alt=""/>
                                        <div class="card">
                                            <p class="card-text">${work.description}</p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn-group">
                                                    <c:if test="${sessionScope.userAccount.role ne null}">
                                                        <form action="show_work" method="post">
                                                            <button type="submit" class="btn btn-sm btn-outline-secondary" name="work_id" value="${work.id}"><fmt:message key="upload.works.view"/></button>
                                                        </form>
                                                        <c:if test="${sessionScope.userAccount.role eq 'Admin'}">
                                                            <form action="fill_work_fields" method="post">
                                                                <button type="submit" class="btn btn-sm btn-outline-secondary" name="work_id" value="${work.id}"><fmt:message key="upload.works.edit"/></button>
                                                            </form>
                                                            <form action="delete" method="post">
                                                                <button type="submit" class="btn btn-sm btn-outline-secondary" name="work_id" value="${work.id}"><fmt:message key="upload.works.delete"/></button>
                                                            </form>
                                                        </c:if>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                    </div>
                </div>
            </div>
        </main>
    </body>
    <script src="assets/dist/js/bootstrap.bundle.min.js"></script>
    <jsp:include page="footer.jsp"/>
</html>
