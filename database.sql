DROP TABLE IF EXISTS work_history;
DROP TABLE IF EXISTS work_employee;
DROP TABLE IF EXISTS userDetails;
DROP TABLE IF EXISTS userAccount;
-- DROP TABLE IF EXISTS picture_work;
DROP TABLE IF EXISTS language;
DROP DATABASE ave;

CREATE DATABASE ave;
\c ave

CREATE TABLE userAccount (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	login VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
	password VARCHAR(50) NOT NULL,
	create_date DATE DEFAULT NULL,
	role VARCHAR(20) NOT NULL,
    banned BOOLEAN NOT NULL
);

CREATE TABLE userDetails (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	firstName VARCHAR(30) NOT NULL,
	lastName VARCHAR(30) NOT NULL,
	gender VARCHAR(6) NOT NULL,
	phone VARCHAR(20) NOT NULL,
    occupation VARCHAR(50) NOT NULL,
    work_experience VARCHAR(10),
    user_id BIGINT REFERENCES userAccount (id)
);

CREATE TABLE work_employee (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	company_name VARCHAR(50) NOT NULL,
    phone_number VARCHAR(20) NOT NULL,
    city VARCHAR(20) NOT NULL,
    street_address VARCHAR(20) NOT NULL,
    home_number  VARCHAR(20) NOT NULL,
    apartment_number VARCHAR(20) NOT NULL,
	description TEXT  NOT NULL,
	payment VARCHAR(50)  NOT NULL,
    picture_name VARCHAR(100),
	user_id BIGINT REFERENCES userAccount (id)
);

-- CREATE TABLE picture_work (
-- 	id BIGSERIAL NOT NULL PRIMARY KEY,
--     picture_name VARCHAR(100) NOT NULL,
--     work_id BIGINT REFERENCES work_employee (id)
-- );

CREATE TABLE work_history (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	user_id BIGINT REFERENCES userAccount (id),
	work_id BIGINT REFERENCES work_employee (id),
    login VARCHAR(20) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    company_name VARCHAR(50) NOT NULL,
    description TEXT  NOT NULL,
    status_work VARCHAR(10) NOT NULL
);

CREATE TABLE language (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	language VARCHAR(10) NOT NULL
);

insert into userAccount (login, email, password, create_date, role, banned) values ('admin', 'sridgedell0@army.mil', '31b69a7494a0eec4ac544fd648c9d604', '2020-12-23', 'Admin', 'false');
insert into userAccount (login, email, password, create_date, role, banned) values ('test', 'cfirbanks1@oaic.gov.au', '31b69a7494a0eec4ac544fd648c9d604', '2020-12-23', 'Employee', 'false');
insert into userAccount (login, email, password, create_date, role, banned) values ('three', 'fcanet2@cyberchimps.com', '31b69a7494a0eec4ac544fd648c9d604', '2020-12-23', 'Employee', 'false');
insert into userAccount (login, email, password, create_date, role, banned) values ('four', 'kahmad3@cisco.com', '31b69a7494a0eec4ac544fd648c9d604', '2020-12-23', 'Employee', 'false');
insert into userAccount (login, email, password, create_date, role, banned) values ('five', 'tcook4@ebay.co.uk', '31b69a7494a0eec4ac544fd648c9d604', '2020-12-23', 'Employee', 'false');

insert into userDetails (firstname, lastname, gender, phone, occupation, work_experience, user_id) values ('Sioux', 'Ridgedell', 'Male', '295-407-8158', 'Строитель', '3', '1');
insert into userDetails (firstname, lastname, gender, phone, occupation, work_experience, user_id) values ('Cecilla', 'Firbanks', 'Male', '818-982-7425', 'Сварщик', '10', '2');
insert into userDetails (firstname, lastname, gender, phone, occupation, work_experience, user_id) values ('Faustina', 'Canet', 'Male',  '221-157-3223', 'Электрик', '1', '3');
insert into userDetails (firstname, lastname, gender, phone, occupation, work_experience, user_id) values ('Kerwin', 'Ahmad', 'Female', '177-436-2933', 'Программист', '2', '4');
insert into userDetails (firstname, lastname, gender, phone, occupation, work_experience, user_id) values ('Thebault', 'Cook', 'Female', '545-857-9202', 'Бухгалтер', '6', '1');

insert into work_employee (company_name, phone_number, city, street_address, home_number, apartment_number, description, payment, user_id) values ('Quire', '333444', 'Vila', 'Milwaukee Place', '10', '671', 'magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus', '$4.77', '1');
insert into work_employee (company_name, phone_number, city, street_address, home_number, apartment_number, description, payment, user_id) values ('Browsecat', '222555', 'Nova', 'Continental Drive', '87', '446', 'ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac', '$3.49', '1');
insert into work_employee (company_name, phone_number, city, street_address, home_number, apartment_number, description, payment, user_id) values ('Zoombox', '444777', 'Markaz', 'Charing Cross Plaza', '1', '111', 'ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer', '$7.45', '1');
insert into work_employee (company_name, phone_number, city, street_address, home_number, apartment_number, description, payment, user_id) values ('Bluejam', '999111', 'Zhizag', 'Ridge Oak', '84', '4', 'augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst', '$6.76', '1');
insert into work_employee (company_name, phone_number, city, street_address, home_number, apartment_number, description, payment, user_id) values ('Riffwire', '111555', 'Guamal', '8 Leroy ParkWay', '92', '4646', 'arcu sed augue aliquam erat volutpat in congue etiam', '$2.42', '1');

insert into language (id, language) values (1,'en_US'),(2,'ru_RU');


